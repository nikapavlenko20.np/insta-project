import React, { useEffect } from 'react';
import Header from '../../components/Header/Header'
import PostsFeed from '../../components/PostsFeed/PostsFeed';
import Modal from '../../components/Modal/Modal'
import {useSelector} from "react-redux";
import SideBar from '../../components/SideBar/SideBar';
import './HomePage.scss'

const HomePage = () => {
    const modalIsOpen = useSelector(state => state.modal);


    return (
        <>
            {modalIsOpen && <Modal/>}
            <Header/>
            <div className="home">
                <PostsFeed/>
                <SideBar/>
            </div>

        </>
    );
};

export default HomePage;