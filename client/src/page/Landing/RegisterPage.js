import React, {useState, useEffect} from 'react';
import {Formik,  Form, Field} from "formik";
import FormInput from '../../components/FormInput/FormInput';
import schema from './validationSchema'
import loginActions from '../../redux/Actions/userActions'
import { Link } from 'react-router-dom';


import {useDispatch, useSelector} from "react-redux";
import Alert from '../../components/Alert/Alert';

const RegisterPage = ({history}) => {
  const dispatch = useDispatch();
  const errors = useSelector(state => state.errors)
  const handleSubmit = async (values, {setSubmitting, resetForm}) => {
      setSubmitting(true)
    const {name, email, firstName, lastName, password, photo} = values;
    const reader = new FileReader();
    reader.readAsDataURL(photo);
    reader.onloadend = () => {
        dispatch(loginActions.registerAsync({name, email, firstName, lastName, password, photo: reader.result}));
        resetForm();
        setSubmitting(false);
        if (!errors) {
            history.push('/login')
        }
    };
  }
  return (
    <div className="login">
        <Alert/>
        <div className="form">
        <div className="form__inner">
            <div className="form__head">
                <h1 className="form__name">Register</h1>
            </div>
        <Formik
                    initialValues={{ name: '', email: '', firstName: '', lastName: '', password: '', passwordConfirmation: '', photo: '' }}
                    validationSchema={schema.registerSchema}
                    onSubmit={handleSubmit}
                    validateOnBlur={true}
                >
                    {({handleChange, onSubmit, isValid, dirty,  isSubmitting, setFieldValue}) => (
                        <Form>
                            <label className="form__label" htmlFor="name">name</label>
                            <Field
                                component={FormInput}
                                type="text"
                                name="name"
                                onChange={handleChange}
                            />
                            <label className="form__label" htmlFor="email">Email</label>
                            <Field
                                component={FormInput}
                                type="text"
                                name="email"
                                onChange={handleChange}
                            />
                            <label className="form__label" htmlFor="firstName">firstName</label>
                            <Field
                                component={FormInput}
                                type="text"
                                name="firstName"
                                onChange={handleChange}
                            />
                            <label className="form__label" htmlFor="lastName">lastName</label>
                            <Field
                                component={FormInput}
                                type="text"
                                name="lastName"
                                onChange={handleChange}
                            />
                          
                            <label className="form__label" htmlFor="password">Password</label>
                            <Field
                                component={FormInput}
                                type="password"
                                name="password"
                                onChange={handleChange}
                            />
                            <label className="form__label" htmlFor="passwordConfirmation">passwordConfirmation</label>
                            <Field
                                component={FormInput}
                                type="password"
                                name="passwordConfirmation"
                                onChange={handleChange}
                            />
                            <label className="form__label" htmlFor="photo">photo</label>
                            <Field
                                component={FormInput}
                                name="file"
                                type="file"
                                onChange={(event) => {
                                  setFieldValue("photo", event.currentTarget.files[0])}}
                            />
                            <button type="submit" disabled={!isValid || !dirty || isSubmitting}>
                                Register
                            </button>
                        </Form>
                    )}
                </Formik>
        </div>
        </div>

        <div className="link"> <Link to="/login">Login</Link></div>
       

    </div>
  );
};

export default RegisterPage;