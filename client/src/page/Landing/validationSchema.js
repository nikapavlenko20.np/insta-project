import * as yup from "yup";

const loginSchema = yup.object().shape({
    email: yup.string().required("this fill is required").min(2, "Min length is 2"),
    password: yup.string().required("this fill is required"),
    passwordConfirmation: yup.string()
     .oneOf([yup.ref('password'), null], 'Passwords must match')
});

const registerSchema = yup.object().shape({
  email: yup.string().email().required("this fill is required").min(2, "Min length is 2"),
  firstName: yup.string().required("this fill is required"),
  lastName: yup.string().required("this fill is required"),
  password: yup.string().required("this fill is required"),
  passwordConfirmation: yup.string()
   .oneOf([yup.ref('password'), null], 'Passwords must match'),
});



export default {
  loginSchema,
  registerSchema

};
