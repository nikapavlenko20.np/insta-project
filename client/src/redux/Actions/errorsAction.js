import {USER_LOGIN_FAIL,SUBSCRIBE_TO_USER_FAIL,UNSUBSCRIBE_FROM_USER_FAIL,ADD_LIKE_TO_POST_FAIL,REMOVE_LIKE_FROM_POST_FAIL,SAVE_POSTS_FAIL,CREATE_POST_FAIL,SET_SUGGESTION_FAIL,REGISTER_FAIL,USER_FOLLOW_FAIL, USER_UNFOLLOW_FAIL, CLEAR_ERROR_STATE} from "../ActionTypes/errorActionTypes";

const loginError = (error)=> ({
    type: USER_LOGIN_FAIL,
    payload: error.toString()
  })

const registerError = (error) => ({
    type: REGISTER_FAIL,
    payload: error.toString()
})

const suggestionError = (error) => ({
    type: SET_SUGGESTION_FAIL,
    payload: error.toString()
})

const followError = (error) => ({
    type: USER_FOLLOW_FAIL,
    payload: error.toString()
})
const unFollowError = (error) => ({
    type: USER_UNFOLLOW_FAIL,
    payload: error.toString()
})
const clearState = () => ({
    type: CLEAR_ERROR_STATE,
})


export default {
    loginError,
    registerError,
    suggestionError,
    followError,
    unFollowError,
    clearState
}