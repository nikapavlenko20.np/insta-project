import "./Posts.scss";

const PostItem = ({photo, comments, likes, name}) => {
    return (
        <div className="user_post" >
            <div className="user_post__img_container">
                <div className="user_post__info">
                    <div className="user_post__info_icons"><i className="fas fa-heart"/> {likes.length}</div>
                    <div className="user_post__info_icons"><i className="fas fa-comment"/> {comments.length}</div>
                </div>
                <img src={photo} alt={`Post by ${name}`} className="user_post__img"/>

            </div>

        </div>
    );
};

export default PostItem;