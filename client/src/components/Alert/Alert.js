import React, {useState, useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import errorsAction from '../../redux/Actions/errorsAction';
import './Alert.scss';

const Alert = () => {
  
  const dispatch = useDispatch()
  const [isOpen, setIsOpen] = useState(false)
  const errors = useSelector(state => state.errors)
  useEffect(() => {
    if (errors) {
      setIsOpen(true)
      setTimeout(()=>{setIsOpen(false); dispatch(errorsAction.clearState())}, 3500)
    } else {
      setIsOpen(false)
    }
  
    
  }, [errors])
  return (
    <>
      {isOpen ? <div className="alert">  <p className="alert__message">{errors}</p>  </div> : null}
    </>
  );
};

export default Alert;