import React from 'react';

const FormInput =  ({form, field, ...rest}) => {
  const {name} = field;
    return (
        <>
            {
                form.touched[name]
                && form.errors[name]
                && <span className="form__error">{form.errors[name]}</span>
            }
            <input className="form__input" {...field} {...rest} />
        </>
    );

};

export default FormInput;