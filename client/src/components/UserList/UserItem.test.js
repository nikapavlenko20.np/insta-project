import {render, fireEvent} from '@testing-library/react'
import { Provider } from 'react-redux';
import UserItem from './UserItem';
import {BrowserRouter} from 'react-router-dom';
import thunk from "redux-thunk";
import createMockStore from 'redux-mock-store';

const store = createMockStore([thunk])({suggestions: {}});
const data = {
  _id: 1,
  name: "hey",
  photo: "https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Gavia_arctica1.jpg/220px-Gavia_arctica1.jpg"
}
test('user item rendered with subscribe btn', ()=> {
 
  
    const {getByText} = render(<BrowserRouter><Provider store={store}><UserItem key={data._id} user={data} canSub={true} /></Provider></BrowserRouter>)
    expect(getByText('Subscribe').textContent).toBeDefined()
});

test('Subscribe btn called click handler', ()=> {
  const mockedFunction = jest.fn()
  
  
    const {getByTestId} = render(<BrowserRouter><Provider store={store}><UserItem key={data._id} user={data} canSub={true} /></Provider></BrowserRouter>)

    const btn = getByTestId('subscribe-btn');
    fireEvent.click(btn);
    expect(mockedFunction).toBeCalled()

});