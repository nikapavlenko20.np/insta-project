import React, {useState, useRef} from "react";
import "./postHome.scss";
import Icons from "../../svgIcons/Icons";
import {likePost, commentPostFetch} from "../../utils/FetchToServer";
import {useSelector} from "react-redux";
import {Link} from "react-router-dom";


const PostHome = ({data, testfanction}) => {
    const {
        title,
        body,
        photo,
        postedBy: {name: nameUserPost, _id: userId, photo: userPhoto},
        likes: likesUsers,
        comments,
        id: postId
    } = data;
    const [valueComment, setValueComment] = useState("");
    const currentUserName = useSelector(state => state.user.name);
    const index = likesUsers.find((el) => el.name === currentUserName);
    const [like, setLike] = useState(!!index);
    const [allLikeArr, setAllLikeArr] = useState(likesUsers);
    const [commentUser, setCommentUser] = useState(comments);
    const [showComments, setShowComments] = useState(false);
    const commentInput = useRef();

    const handleClickOnLike = async () => {
        await likePost(like, {userId, postId});
        setLike(!like);
        if (!like) {
            setAllLikeArr([...allLikeArr, {name: currentUserName}]);
        } else {
            setAllLikeArr(allLikeArr.filter((el) => el.name !== currentUserName));
        }
    };
    const handleClickToAddComments = async () => {
        const comment = commentInput.current.value;
        await commentPostFetch({
            text: comment,
            postId: postId
        });
        setCommentUser([...commentUser, {user: currentUserName, text: comment}]);
    };

    const controlComments = (index) => {
        if (commentUser.length - 1 === index) return false;
        if (commentUser.length - 2 === index) return false;
        if (commentUser.length - 3 === index) return false;
        return true;
    };

    let commentItemsHidden = commentUser.map((comment, index) => (
        <li className='post__comment-item'
            key={index}
            hidden={showComments ? false : controlComments(index)}>
            <span className='post__comment-author'>{currentUserName}</span>{comment.text}</li>));

    const handleClickShowMoreButton = () => {
        setShowComments(true);
    };


    return (
        <div className='post'>
            <div className='post__header'>
                <Link to={`/user/${userId}`} className='post__link'>
                    <div className='post__author-img-wrapper'>
                        <img data-testid='img-user' className='post__author-img' src={userPhoto} alt="avatar"/>
                    </div>
                    <p className='post__author-name'>{nameUserPost}</p>
                </Link>
            </div>
            <div data-testid='photo-test-wrapper' className='post__photo-wrapper'
                 onDoubleClick={testfanction || handleClickOnLike}>
                <img className='post__photo' data-testid='img-post' src={photo} alt='photo'/>
            </div>
            <div className='post__like-wrapper'>
                <Icons data-testid='like-img-photo' type={like ? "heartLike" : "heartUnlike"} classes='post__like'
                       color='red' onClick={testfanction || handleClickOnLike}/>
                {/*<p className='post__like-number'>{like || !index ? likesUsers.length : likesUsers.length-1} отметок Нравится</p>*/}
                {/*<p className='post__like-number'>{like ? likesUsers.length+1 : likesUsers.length } отметок Нравится</p>*/}
                <p className='post__like-number'>{allLikeArr.length} отметок Нравится</p>
            </div>
            <div className='post__content'>
                <p className='post__author-name'>{nameUserPost}</p>
                <p className='post__title'>{title}</p>
                <p className='post__text'>{body}</p>
                <ul className='post__comment-list'>
                    {commentUser.length > 3 && !showComments &&
                    <button data-testid='bth-show-more-comments' className='post__bth-more-comment'
                            onClick={testfanction || handleClickShowMoreButton}>show more comments</button>}
                    {commentItemsHidden}
                </ul>
            </div>
            <div className='post__set-comment-user'>
                <input className='post__input-comment' data-testid='input-create-new-comment' type="text"
                       placeholder='add comments' onChange={testfanction || ((e) => setValueComment(e.target.value))}
                       value={valueComment} ref={commentInput}/>
                <button className='post__bth-send-comment' data-testid='bth-create-new-comment' disabled={!valueComment}
                        onClick={testfanction || handleClickToAddComments}>Send
                </button>
            </div>
        </div>
    );
};


export default PostHome;