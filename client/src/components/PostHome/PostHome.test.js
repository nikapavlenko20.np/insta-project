import PostHome from "./PostHome";
import createMockStore from 'redux-mock-store';
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import {fireEvent} from "@testing-library/react";
import React from "react";

const store = createMockStore([thunk])({user: {
    name:'hhh'
    }});

jest.mock('../../svgIcons/Icons', ()=>()=><p>img</p>);


const posts = {
        id:"600c61b0223a26077da9ce28",
        title:"df",
        body:"dsf",
        photo:"https://res.cloudinary.com/dnhomnole/image/upload/v1611424176/t5okgwanocgopfyr3dxs.png",
        postedBy: {
            _id:"5ffeb75149ed9d82e859c6ea",
            name: "dsfds",
            photo:"//www.gravatar.com/avatar/b8be581ff827b6f0548dce6830848a98?s=200&r=pg&d=mm"
        },
        likes: [],
        comments: []
};

const postWithComment = {
    id:"600c61b0223a26077da9ce28",
    title:"df",
    body:"dsf",
    photo:"https://res.cloudinary.com/dnhomnole/image/upload/v1611424176/t5okgwanocgopfyr3dxs.png",
    postedBy: {
        _id:"5ffeb75149ed9d82e859c6ea",
        name: "dsfds",
        photo:"//www.gravatar.com/avatar/b8be581ff827b6f0548dce6830848a98?s=200&r=pg&d=mm"
    },
    likes: [],
    comments: [
        {
            _id:"600d7299f8cf9e0c4d84b635",
            text:"sdf"
        },
        {
            _id:"600d7299f8cf9e0c4d84b635",
            text:"sdf"
        },
        {
            _id:"600d7299f8cf9e0c4d84b635",
            text:"sdf"
        },
        {
            _id:"600d7299f8cf9e0c4d84b635",
            text:"sdf"
        }
        ]
}



test('PostHome render', ()=>{

    const {getByText, getByTestId, getAllByText} = render(<Provider store={store}><PostHome data={posts}/></Provider>);

    expect(getByText('img').textContent).toBeDefined();
    expect(getByTestId('img-post')).toBeDefined();
    expect(getByTestId('img-user')).toBeDefined();
    expect(getAllByText('dsfds')).toBeDefined();
    expect(getByText('img')).toBeDefined();
    expect(getByTestId('bth-create-new-comment').textContent).toBe('Send');
    expect(getByTestId('input-create-new-comment')).toBeDefined();
});


test('testing double click on photo post', ()=> {
    const clickMock= jest.fn();
    const { getByTestId} = render(<Provider store={store}><PostHome testfanction={clickMock} data={posts}/></Provider>);

    const photo = getByTestId('photo-test-wrapper');

    expect(photo).toBeDefined();

    fireEvent.doubleClick(photo);
    expect(clickMock).toBeCalled();
});

test('testing click on show comment post', ()=> {
    const clickMock= jest.fn();
    const { getByTestId} = render(<Provider store={store}><PostHome testfanction={clickMock} data={postWithComment}/></Provider>);

    const bth = getByTestId('bth-show-more-comments');

    expect(bth).toBeDefined();
    fireEvent.click(bth);

    expect(clickMock).toBeCalled();
});

test('testing click on send comment post', ()=> {
    const clickMock= jest.fn();
    const { getByTestId} = render(<Provider store={store}><PostHome testfanction={clickMock} data={postWithComment}/></Provider>);

    const bth = getByTestId('bth-create-new-comment');

    expect(bth).toBeDefined();
    fireEvent.click(bth);

    expect(clickMock).toBeCalled();
})








