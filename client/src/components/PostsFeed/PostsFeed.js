import React, {useEffect, useState} from "react";
import PostHome from "../PostHome/PostHome";
import {useDispatch, useSelector} from "react-redux";
import postActions from "../../redux/Actions/postActions";
import "./PostsFeed.scss";
import InfiniteScroll from "react-infinite-scroll-component";

const PostsFeed = () => {
    const dispatch = useDispatch();
    const allPosts = useSelector(state => state.post);
    const [currentPage, setCurrentPage] = useState(1);

    useEffect(() => {
        dispatch(postActions.saveAllPostFromUserAsync(currentPage));
    }, []);


    useEffect(() => {
        dispatch(postActions.saveAllPostFromUserAsync(currentPage));
    }, [currentPage]);


    const fetchMoreData = () => {
        setCurrentPage(currentPage + 1);
    };

    const cardItems = allPosts.map(post => <PostHome key={post.id} data={post}/>);

    return (
        <InfiniteScroll
            className='post-wrapper'
            dataLength={allPosts.length}
            next={() => fetchMoreData()}
            hasMore={true}
            endMessage={
                <p style={{textAlign: "center"}}>
                    <b>Yay! You have seen it all</b>
                </p>
            }
        >
            {cardItems}

        </InfiniteScroll>
    );
};

export default PostsFeed;