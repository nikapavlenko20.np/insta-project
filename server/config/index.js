import dotenv from 'dotenv';

const enviroment = dotenv.config();

if (!enviroment) {
  throw new Error('Config file was not found');
}

export default {
  PORT: process.env.PORT || 3005,
  SERVICE_PREFIX: process.env.SERVICE_PREFIX || '/api',
  VERSION: process.env.VERSION || 1,
  MONGODB_URI: process.env.MONGODB_URI || null,
  JWT_SECRET: process.env.JWT_SECRET || '',
  CLOUD_NAME: process.env.CLOUD_NAME || '',
  CLOUD_API_KEY: process.env.CLOUD_API_KEY || '',
  CLOUD_API_SECRET: process.env.CLOUD_API_SECRET || '',
}